<?php

declare(strict_types=1);

namespace Talentry\SlackBundle\Tests;

use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\Slack\SlackClient;

class IntegrationTest extends KernelTestCase
{
    private const string API_TOKEN_IN_BUNDLE_CONFIG = 'foo'; //configured in tests/config.yml

    private SlackClient $slackClient;

    protected function setUp(): void
    {
        $this->slackClient = self::getContainer()->get(SlackClient::class);
    }

    public function testApiTokenIsConfigured(): void
    {
        $rc = new ReflectionClass($this->slackClient);
        $prop = $rc->getProperty('slackApiToken');
        $prop->setAccessible(true);
        self::assertSame(self::API_TOKEN_IN_BUNDLE_CONFIG, $prop->getValue($this->slackClient));
    }
}
