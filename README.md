# Slack bundle

This is a Symfony bundle used for integrating talentrydev/slack library into a Symfony project.

## Installing

* Run:

```
composer require talentrydev/slack-bundle
```

* Add the SlackBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\SlackBundle\SlackBundle();
];
```

# Configuring

You must provide the slack API token to the bundle. Obtain the token from slack and then configure the bundle like so:

```
slack:
  api_token: foo
```
