PHP_IMAGE=php:8.3-cli

test:
	docker run --rm -v $(shell pwd):/app -w /app -t $(PHP_IMAGE) vendor/bin/phpunit
cs:
	docker run --rm -v $(shell pwd):/app -w /app -t $(PHP_IMAGE) vendor/bin/phpcs --standard=PSR12 src tests
cs-fix:
	docker run --rm -v $(shell pwd):/app -w /app -t $(PHP_IMAGE) vendor/bin/phpcbf --standard=PSR12 src tests
deps:
	docker run -v $(shell pwd):/app --rm -t composer install --ignore-platform-reqs
