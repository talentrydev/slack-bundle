<?php

declare(strict_types=1);

namespace Talentry\SlackBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SlackBundle extends Bundle
{
}
