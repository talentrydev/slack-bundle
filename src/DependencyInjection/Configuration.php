<?php

declare(strict_types=1);

namespace Talentry\SlackBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private const string ROOT_NODE = 'slack';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('api_token')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
