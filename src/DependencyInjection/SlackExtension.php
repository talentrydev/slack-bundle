<?php

declare(strict_types=1);

namespace Talentry\SlackBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Talentry\Slack\SlackClient;

class SlackExtension extends Extension
{
    /**
     * @param array<mixed> $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $env = $container->getParameter('kernel.environment');
        if ($env === 'test') {
            $loader->load('services_test.yml');
        }

        $config = $this->processConfiguration(new Configuration(), $configs);
        $controller = $container->getDefinition(SlackClient::class);
        $controller->setArgument('$slackApiToken', $config['api_token']);
    }
}
